package com.example.springboot.cloud.config.client.poc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExampleConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    /**
     * Constructor
     * @param someValue retrieved from cloud config server
     */
    public ExampleConfiguration(@Value("${some.property.value}") final String someValue) {
        LOGGER.info("...");
        LOGGER.info("...");
        LOGGER.info("...");
        LOGGER.info("some property value : {}", someValue);
        LOGGER.info("...");
        LOGGER.info("...");
        LOGGER.info("...");
    }
}
